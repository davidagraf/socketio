var express = require('express');
var app = express();
var path = require('path');

app.get('/test', function(req, res) {
  res.send('Hello Test');
});

app.use(express.static(path.join(__dirname, 'public')));
app.set('domain', '127.0.0.1');
app.listen(3000);
console.log('listening on port 3000');

var io = require('socket.io').listen(9000);

io.sockets.on('connection', function(socket) {
  socket.on('chat', function(data) {
    io.sockets.emit('append', data);
  });
});
